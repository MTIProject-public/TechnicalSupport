const config = {
  WEBAPP_URI: 'https://stage-mti-project.herokuapp.com/',
  // WEBAPP_URI: 'http://localhost:3000/',
  BROWSER_CLI: 'google-chrome',
  //  BROWSER_CLI: 'chromium-browser',
  'ad.anynet.pwd_hash': '90c2694b7a1d90c19f93468d9aea6ba1237b32a9958bd1bbe4c1016c7358cbe6',
  'ad.anynet.pwd_salt': '29f1790e6d21abf758338c7cdfa8bf59',
};

export default config;
