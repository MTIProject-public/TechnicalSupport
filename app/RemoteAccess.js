import os from 'os';
import readline from 'readline';
import fs from 'fs';
import { spawn } from 'child_process';
import config from './util/config';

export const publishAnydeskId = () => {
  const osUser = os.userInfo().username;
  const systemFile = {};

  const lineReader = readline.createInterface({
    input: fs.createReadStream(`/home/${osUser}/.anydesk/system.conf`),
  });

  lineReader.on('line', (line) => { // can't use split, need to support '=' in value
    const breakPoint = line.indexOf('=');
    const key = line.substring(0, breakPoint);
    const value = line.substr(breakPoint + 1);
    systemFile[key] = value;
  });

  lineReader.on('close', () => {
    const anydeskId = systemFile['ad.anynet.id'];
    systemFile['ad.security.interactive_access'] = 0;
    // make sure interactive mode is 0
    const newSystemFile = fs.createWriteStream(`/home/${osUser}/.anydesk/system.conf`);
    Object.keys(systemFile).forEach((key) => {
      newSystemFile.write(`${key}=${systemFile[key]}${os.EOL}`);
    });
    const chrome = spawn(config.BROWSER_CLI, [`${config.WEBAPP_URI}public/TechnicalSupport.html?anydesk=${anydeskId}`]);
    chrome.on('error', (e) => {
      console.log(`running ${config.BROWSER_CLI} error`, e);
      console.log('**** executing continues ****');
    });
  });
};

// need to set Anydesk configuration such that the password is constant
// after grabbing the AnyDesk ID
export const adjustAnyDeskParams = () => {
  const osUser = os.userInfo().username;
  const serviceFile = {};

  const lineReader = readline.createInterface({
    input: fs.createReadStream(`/home/${osUser}/.anydesk/service.conf`),
  });

  lineReader.on('line', (line) => { // can't use split, need to support '=' in value
    const breakPoint = line.indexOf('=');
    const key = line.substring(0, breakPoint);
    const value = line.substr(breakPoint + 1);
    serviceFile[key] = value;
  });

  lineReader.on('close', () => {
    serviceFile['ad.anynet.pwd_hash'] = config['ad.anynet.pwd_hash'];
    serviceFile['ad.anynet.pwd_salt'] = config['ad.anynet.pwd_salt'];
    const newServiceFile = fs.createWriteStream(`/home/${osUser}/.anydesk/service.conf`);
    Object.keys(serviceFile).forEach((key) => {
      newServiceFile.write(`${key}=${serviceFile[key]}${os.EOL}`);
    });
  });
};


export const verifyLoginScript = () => {
  const osUser = os.userInfo().username;
  const loginScriptFile = [];

  const lineReader = readline.createInterface({
    input: fs.createReadStream(`/home/${osUser}/.profile`),
  });

  let isFoundOurs = false;

  lineReader.on('line', (line) => {
    if (line.indexOf('TechnicalSupport') > -1) {
      isFoundOurs = true;
    }
    loginScriptFile.push(line);
  });

  lineReader.on('close', () => {
    if (!isFoundOurs) {
      const newServiceFile = fs.createWriteStream(`/home/${osUser}/.profile`);
      loginScriptFile.push(`${os.EOL}cd /home/${osUser}/TechnicalSupport && npm start && cd -${os.EOL}`);
      loginScriptFile.forEach((line) => {
        newServiceFile.write(`${line}${os.EOL}`);
      });
    }
  });
};
