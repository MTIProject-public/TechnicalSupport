import { publishAnydeskId, adjustAnyDeskParams, verifyLoginScript } from './app/RemoteAccess';

publishAnydeskId();
adjustAnyDeskParams();
verifyLoginScript();
